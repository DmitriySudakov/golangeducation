package repositories

import (
	"encoding/csv"
	"fmt"
	"goLangEducation/go_04/dev.dimas.UserService/models"
	"os"
	"strconv"
)

type UserRepository struct{}

func (ur UserRepository) SaveUser(user models.User) models.User {
	file, err := os.OpenFile("users.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Failed to open file:", err)
		return models.User{}
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	record := []string{
		strconv.Itoa(user.ID),
		user.FirstName,
		user.LastName,
		fmt.Sprintf("%d", user.Age),
		user.Email,
		user.Password,
	}

	err = writer.Write(record)
	if err != nil {
		fmt.Println("Произошла ошибка:", err)
		return models.User{}
	}

	return user
}
