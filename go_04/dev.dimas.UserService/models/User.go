package models

import "math/rand"

type User struct {
	ID        int
	FirstName string
	LastName  string
	Age       int
	Email     string
	Password  string
}

func NewUser(firstName, lastName, email, password string, age int) User {
	id := rand.Int()
	return User{
		ID:        id,
		FirstName: firstName,
		LastName:  lastName,
		Age:       age,
		Email:     email,
		Password:  password,
	}
}
