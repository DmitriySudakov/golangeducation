package services

import (
	"fmt"
	"goLangEducation/go_04/dev.dimas.UserService/models"
	"goLangEducation/go_04/dev.dimas.UserService/repositories"
)

type UserService struct {
	userRepository repositories.UserRepository
}

func NewUserService(repository repositories.UserRepository) *UserService {
	return &UserService{userRepository: repository}
}

func (us UserService) RegisterUser(user models.User) models.User {
	err := us.userRepository.SaveUser(user)
	if err != nil {
		fmt.Println("Failed to save user:", err)
		return models.User{}
	}
	return user
}
