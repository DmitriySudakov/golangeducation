package controllers

import (
	"bufio"
	"fmt"
	"goLangEducation/go_04/dev.dimas.UserService/models"
	"goLangEducation/go_04/dev.dimas.UserService/services"
	"os"
	"strings"
)

type UserController struct {
	UserService services.UserService
}

func NewUserController(userService services.UserService) *UserController {
	return &UserController{UserService: userService}
}

func (uc UserController) RegisterUserFromConsole() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter First Name: ")
	firstName, _ := reader.ReadString('\n')
	firstName = strings.TrimSpace(firstName)

	fmt.Print("Enter Last Name: ")
	lastName, _ := reader.ReadString('\n')
	lastName = strings.TrimSpace(lastName)

	fmt.Print("Enter Email: ")
	email, _ := reader.ReadString('\n')
	email = strings.TrimSpace(email)

	fmt.Print("Enter Password: ")
	password, _ := reader.ReadString('\n')
	password = strings.TrimSpace(password)

	fmt.Print("Enter Age: ")
	var age int
	fmt.Scanf("%d", &age)

	user := models.NewUser(firstName, lastName, email, password, age)

	user = uc.UserService.RegisterUser(user)

	fmt.Println("User registered successfully!")

}
