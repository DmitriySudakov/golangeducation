package main

import (
	"fmt"
	"time"
)

type Document struct {
	Description string
	Title       string
	Copies      int
	StartDate   time.Time
	EndDate     time.Time
}

func (d *Document) ChangeDescription(newDescription string) {
	d.Description = newDescription
}

func (d *Document) ChangeCopies(newCopies int) {
	if newCopies >= 0 {
		d.Copies = newCopies
	}
}

func (d *Document) PrintInfo() {
	fmt.Println("Title:", d.Title)
	fmt.Println("Description:", d.Description)
	fmt.Println("Copies:", d.Copies)
	fmt.Println("Start Date:", d.StartDate.Format("2006-01-02"))
	fmt.Println("End Date:", d.EndDate.Format("2006-01-02"))
}

func main() {
	doc := Document{
		Description: "Sample Document",
		Title:       "Document Title",
		Copies:      5,
		StartDate:   time.Now(),
		EndDate:     time.Now().AddDate(0, 0, 7),
	}
	doc.ChangeDescription("Updated Document")
	doc.ChangeCopies(10)
	doc.PrintInfo()
}
