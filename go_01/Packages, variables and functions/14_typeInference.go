package main

import "fmt"

func main() {
	v := float64(42)
	fmt.Printf("v is of type %T\n", v)
}
