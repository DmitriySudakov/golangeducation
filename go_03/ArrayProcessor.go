package main

import (
	"fmt"
)

type ArrayProcessor interface {
	ArrayToArray(arr []int) []int
	ArrayToInt(arr []int) int
}

type SumAndZeroProcessor struct{}

func (sp SumAndZeroProcessor) ArrayToArray(arr []int) []int {
	result := []int{}
	for _, num := range arr {
		if num != 0 {
			result = append(result, num)
		}
	}
	return result
}

func (sp SumAndZeroProcessor) ArrayToInt(arr []int) int {
	sum := 0
	for _, num := range arr {
		sum += num
	}
	return sum
}

type MultAndNegativeProcessor struct{}

func (mp MultAndNegativeProcessor) ArrayToArray(arr []int) []int {
	result := []int{}
	for _, num := range arr {
		if num >= 0 {
			result = append(result, num)
		}
	}
	return result
}

func (mp MultAndNegativeProcessor) ArrayToInt(arr []int) int {
	product := 1
	for _, num := range arr {
		product *= num
	}
	return product
}

func ProcessArray(arr []int, processor ArrayProcessor) ([]int, int) {
	processedArray := processor.ArrayToArray(arr)
	number := processor.ArrayToInt(arr)
	return processedArray, number
}

func main() {
	arr := []int{1, -2, 3, 0, 5}
	sp := SumAndZeroProcessor{}
	sumArr, sum := ProcessArray(arr, sp)
	fmt.Println("Array without zeros:", sumArr)
	fmt.Println("Sum of array elements:", sum)

	mp := MultAndNegativeProcessor{}
	posArr, product := ProcessArray(arr, mp)
	fmt.Println("Array without negative elements:", posArr)
	fmt.Println("Product of array elements:", product)
}
